#ifndef tcptap_local_h
#define tcptap_local_h
#include <limits.h>
#include <pthread.h>
#include <stdbool.h>

/* Stringify */
#define _XSTR( X ) #X
#define STR( X ) _XSTR( X )

/* File-descriptor for READ end of a pipe */
#define PIPE_RD( P ) ( P[0] )
/* File-descriptor for WRITE end of a pipe */
#define PIPE_WR( P ) ( P[1] )

/* Environment overloadable variables. Note: NEVER change these in code
 * unless bug is found as they are considered trusted and safe, and as they
 * will allow tcp_tap to start without any wrapping scripts (important when
 * debugging and testing). Always use the corresponding environment variable
 * */
struct env {
/* Name of the main process to run */
    char execute_bin[NAME_MAX];

/* Port number */
    char port[NAME_MAX];

/* FIFO(s) pre-name */
    char fifo_prename[NAME_MAX];

/* listen at NIC bound to this name (human readable name or
 * IP-address). Aditionaly two special names:
 * @HOSTNAME@: Look up the primary interface bound to this name
 * @ANY@: Allow connection to any of the servers IF
 * */
    char nic_name[NAME_MAX];

/* Endpoint data-transfer logging
 * */
    bool enable_log_stdin_to_cmd;
    bool enable_log_cmd_to_stdout;
    bool enable_log_taps_to_cmd;
};

/* Transfer between two fd end-poins */
struct data_link {
    int read_from;
    int write_to;
    int enable_log;
    const char *worker_name;
    char *buffer;
    struct link *link;
};

typedef enum {
    stdin_to_cmd = 0,
    cmd_to_stdout,
    taps_to_cmd,
    n_links
} links_t;

enum link_status {
    link_idle = 0,
    link_starting,
    link_up,
    link_exiting,
    link_killed,
    link_dead,
};

struct link {
    enum link_status status;
    pthread_t thid;
    void *(*worker)(void *);
    struct data_link *data_link;
};

/*
 * Project internal funtions
 */
void env_int(void);
struct env *env_get();
void *thread_stdin_to_cmd(void *arg);
void *thread_cmd_to_stdout(void *arg);
void *thread_taps_to_cmd(void *arg);
struct link *link_create(void *(*worker)(void *), int fd_from, int fd_to);
void link_kill(struct link *);
int parent_child(int argc, char *[]);

void tty_init();
void tty_fini();
void tty_raw_mode(int fd);

#ifdef __ANDROID__

/*  <android/log.h> is part of NDK <cutils/log.h> is not. <android/log.h>
    however lacks the LOGX macros */

#include <android/log.h>

#ifndef  LOG_TAG
#define  LOG_TAG "tcp-tap"
#endif

#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#else

#include <liblog/log.h>

#endif                          // __ANDROID__

#endif                          //tcptap_local_h
