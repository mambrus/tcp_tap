/***************************************************************************
 *   Copyright (C) 2011 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include "sig_mngr.h"
#include "local.h"

#include <liblog/assure.h>
#include "sig_mngr.h"
#include "tcp-tap_config.h"

static pid_t _pid_child = -1;

/* Install signal-handler for every imaginable signal. If tcp_tap receives
 * any, its default behaviour is to forward it to the child, unless
 * corresponding environment variable says differently.
 */
#define DEF_FWD(X)                                          \
    void mngSig_##X( int sig ) {                            \
        signal( X, SIG_IGN);                                \
        LOGW("Forwarding signal [" #X "]\n");               \
        kill(_pid_child, sig);                              \
        signal( X, mngSig_##X );                            \
    }

/* Logged but ignored signals Signals are kill() directives. I.e.
 * slave-process usually will die from them. Use this handler to NOT forward
 * the signal, just log it.
 */
#define DEF_IGN(X)                                          \
    void sigIgnore_##X( int sig ) {                         \
        signal( X, SIG_IGN);                                \
        LOGW("Ignoring signal [" #X "]\n");                 \
    }

/* Expand to signal handler-code. Unused handlers will be optimized away by
 * the linker
 */

/* Signal forward handlers */
DEF_FWD(SIGHUP);
DEF_FWD(SIGINT);
DEF_FWD(SIGQUIT);
DEF_FWD(SIGILL);
DEF_FWD(SIGTRAP);
DEF_FWD(SIGABRT);
DEF_FWD(SIGIOT);
DEF_FWD(SIGBUS);
DEF_FWD(SIGFPE);
DEF_FWD(SIGKILL);
DEF_FWD(SIGUSR1);
DEF_FWD(SIGSEGV);
DEF_FWD(SIGUSR2);
DEF_FWD(SIGPIPE);
DEF_FWD(SIGALRM);
DEF_FWD(SIGTERM);
DEF_FWD(SIGSTKFLT);
DEF_FWD(SIGCHLD);
DEF_FWD(SIGCONT);
DEF_FWD(SIGSTOP);
DEF_FWD(SIGTSTP);
DEF_FWD(SIGTTIN);
DEF_FWD(SIGTTOU);
DEF_FWD(SIGURG);
DEF_FWD(SIGXCPU);
DEF_FWD(SIGXFSZ);
DEF_FWD(SIGVTALRM);
DEF_FWD(SIGPROF);
DEF_FWD(SIGWINCH);
DEF_FWD(SIGIO);
DEF_FWD(SIGPOLL);
DEF_FWD(SIGPWR);
DEF_FWD(SIGSYS);
#ifdef HAVE_SIGUNUSED
DEF_FWD(SIGUNUSED);
#endif
DEF_FWD(SIGRTMIN);
DEF_FWD(SIGRTMAX);
//DEF_FWD(SIGSWI);
DEF_FWD(SIGSTKSZ);

/* Signal nuke handlers */
DEF_IGN(SIGHUP);
DEF_IGN(SIGINT);
DEF_IGN(SIGQUIT);
DEF_IGN(SIGILL);
DEF_IGN(SIGTRAP);
DEF_IGN(SIGABRT);
DEF_IGN(SIGIOT);
DEF_IGN(SIGBUS);
DEF_IGN(SIGFPE);
DEF_IGN(SIGKILL);
DEF_IGN(SIGUSR1);
DEF_IGN(SIGSEGV);
DEF_IGN(SIGUSR2);
DEF_IGN(SIGPIPE);
DEF_IGN(SIGALRM);
DEF_IGN(SIGTERM);
DEF_IGN(SIGSTKFLT);
DEF_IGN(SIGCHLD);
DEF_IGN(SIGCONT);
DEF_IGN(SIGSTOP);
DEF_IGN(SIGTSTP);
DEF_IGN(SIGTTIN);
DEF_IGN(SIGTTOU);
DEF_IGN(SIGURG);
DEF_IGN(SIGXCPU);
DEF_IGN(SIGXFSZ);
DEF_IGN(SIGVTALRM);
DEF_IGN(SIGPROF);
DEF_IGN(SIGWINCH);
DEF_IGN(SIGIO);
DEF_IGN(SIGPOLL);
DEF_IGN(SIGPWR);
DEF_IGN(SIGSYS);
#ifdef HAVE_SIGUNUSED
DEF_IGN(SIGUNUSED);
#endif
DEF_IGN(SIGRTMIN);
DEF_IGN(SIGRTMAX);
//DEF_IGN(SIGSWI);
DEF_IGN(SIGSTKSZ);

/* Install sighandler conditioned by environment variable
 * (will be conditional, for now: unconditional)
 */
#define COND_SIGHNDLR_INSTALL(X)                            \
{                                                           \
    signal( X , mngSig_##X );                               \
}

/* Install sighanler for signal to ignore
 */
#define IGNO_SIGHNDLR_INSTALL(X)                            \
{                                                           \
    signal( X , sigIgnore_##X );                            \
}
int sig_mngr_init(pid_t pid_child)
{
    _pid_child = pid_child;

    COND_SIGHNDLR_INSTALL(SIGHUP);
    COND_SIGHNDLR_INSTALL(SIGINT);
    COND_SIGHNDLR_INSTALL(SIGQUIT);
    COND_SIGHNDLR_INSTALL(SIGILL);
    COND_SIGHNDLR_INSTALL(SIGTRAP);
    COND_SIGHNDLR_INSTALL(SIGABRT);
    COND_SIGHNDLR_INSTALL(SIGIOT);
    COND_SIGHNDLR_INSTALL(SIGBUS);
    COND_SIGHNDLR_INSTALL(SIGFPE);
    COND_SIGHNDLR_INSTALL(SIGKILL);
    COND_SIGHNDLR_INSTALL(SIGUSR1);
    COND_SIGHNDLR_INSTALL(SIGSEGV);
    COND_SIGHNDLR_INSTALL(SIGUSR2);
    COND_SIGHNDLR_INSTALL(SIGPIPE);
    COND_SIGHNDLR_INSTALL(SIGALRM);
    COND_SIGHNDLR_INSTALL(SIGTERM);
    COND_SIGHNDLR_INSTALL(SIGSTKFLT);
    IGNO_SIGHNDLR_INSTALL(SIGCHLD);
    COND_SIGHNDLR_INSTALL(SIGCONT);
    COND_SIGHNDLR_INSTALL(SIGSTOP);
    COND_SIGHNDLR_INSTALL(SIGTSTP);
    COND_SIGHNDLR_INSTALL(SIGTTIN);
    COND_SIGHNDLR_INSTALL(SIGTTOU);
    COND_SIGHNDLR_INSTALL(SIGURG);
    COND_SIGHNDLR_INSTALL(SIGXCPU);
    COND_SIGHNDLR_INSTALL(SIGXFSZ);
    COND_SIGHNDLR_INSTALL(SIGVTALRM);
    COND_SIGHNDLR_INSTALL(SIGPROF);
    IGNO_SIGHNDLR_INSTALL(SIGWINCH);
    COND_SIGHNDLR_INSTALL(SIGIO);
    COND_SIGHNDLR_INSTALL(SIGPOLL);
    COND_SIGHNDLR_INSTALL(SIGPWR);
    COND_SIGHNDLR_INSTALL(SIGSYS);
#ifdef HAVE_SIGUNUSED
    COND_SIGHNDLR_INSTALL(SIGUNUSED);
#endif
    COND_SIGHNDLR_INSTALL(SIGRTMIN);
    COND_SIGHNDLR_INSTALL(SIGRTMAX);
    //COND_SIGHNDLR_INSTALL(SIGSWI);
    COND_SIGHNDLR_INSTALL(SIGSTKSZ);

    return 0;
}
