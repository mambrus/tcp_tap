/***************************************************************************
 *   Copyright (C) 2011 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Thread-based shuffling of data between two fd-based end-points This is a
   high abstraction data shuffler where end-points are pipes, stdio e.t.c. I.e.
   data is already serialized and joined if origination from several sources
   Note: heavy lifting mixing is in switch-board.c
*/

#include <stdio.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <errno.h>
#include "tcp-tap/switchboard.h"
#include "sig_mngr.h"
#include "tcp-tap_config.h"
#include "local.h"

#include <liblog/assure.h>

/* The size of each buffer used for transfer in either direction */
#ifndef LINK_BUFF_SZ
#define LINK_BUFF_SZ 0x400
#endif

#ifndef NAME_MAX
#define NAME_MAX 128
#endif                          //NAME_MAX

/* Transfers stdin and sends to child (i.e. the supervised cmd) and TCP
 * tap-sessions, if any. */
void *thread_stdin_to_cmd(void *arg)
{
    int i = -1, j, wfd;
    struct data_link *lp = (struct data_link *)arg;
    char *pname = switchboard_fifo_names()->in_name;
    char name[NAME_MAX + 128];
    char *exec_name = env_get()->execute_bin;
    int n;
    for (n = strlen(exec_name); n > 0 && exec_name[n] != '/'; n--) ;
    exec_name = exec_name[n] != '/' ? &exec_name[n] : &exec_name[n + 1];
    snprintf(name, NAME_MAX, "stdin_to_%s", exec_name);

    lp->enable_log = env_get()->enable_log_stdin_to_cmd;
    lp->worker_name = name;
    LOGD("Worker [%s] starting. Writes to pipe: %s\n", lp->worker_name, pname);

    ASSURE((wfd = open(pname, O_WRONLY)) >= 0);
    LOGD("Worker [%s] fds: (%d->{%d,%d})\n",
         lp->worker_name, lp->read_from, lp->write_to, wfd);

    lp->link->status = link_up;
    while (i) {
        memset(lp->buffer, 0, LINK_BUFF_SZ);
        i = read(lp->read_from, lp->buffer, LINK_BUFF_SZ);
        if (i < 0) {
            LOGE("Failed reading from fd=[%d] in %s: " __FILE__ ":"
                 STR(__LINE__), __func__, lp->read_from);
            exit(EXIT_FAILURE);
        }
        if (lp->enable_log)
            LOGV("R(%s[%d]): [%s]\n", __func__, i, lp->buffer);
        if (!i) {
            LOGW("R(%s[%d]): EOF (^D) received\n", __func__, i);
        }

        j = write(lp->write_to, lp->buffer, i);
        ASSURE(i == j);
        j = write(wfd, lp->buffer, i);
        ASSURE(i == j);
    }
    lp->link->status = link_exiting;
    LOGE("Worker [%s] exits, closing write-pipe: %s\n", lp->worker_name, pname);
    ASSURE(close(wfd) >= 0);
    return (void *)(intptr_t) i;
}

/* Transfers output from child and sends to stdout (and TCP sockets, if any) */
void *thread_cmd_to_stdout(void *arg)
{
    int i = -1, j, wfd;
    struct data_link *lp = (struct data_link *)arg;
    char *pname = switchboard_fifo_names()->in_name;
    char name[NAME_MAX];
    char *exec_name = env_get()->execute_bin;
    int n;
    for (n = strlen(exec_name); n > 0 && exec_name[n] != '/'; n--) ;
    exec_name = exec_name[n] != '/' ? &exec_name[n] : &exec_name[n + 1];
    snprintf(name, NAME_MAX, "%s_to_stdout", exec_name);

    lp->enable_log = env_get()->enable_log_cmd_to_stdout;
    lp->worker_name = name;
    LOGD("Worker [%s] starting. Writes to pipe: %s\n", lp->worker_name, pname);

    ASSURE((wfd = open(pname, O_WRONLY)) >= 0);
    LOGD("Worker [%s] fds: (%d->{%d,%d})\n",
         lp->worker_name, lp->read_from, lp->write_to, wfd);

    lp->link->status = link_up;
    while (i) {
        memset(lp->buffer, 0, LINK_BUFF_SZ);
        i = read(lp->read_from, lp->buffer, LINK_BUFF_SZ);
        if (i < 0) {
            LOGE("Failed reading from fd=[%d] in %s: " __FILE__ ":"
                 STR(__LINE__), __func__, lp->read_from);
            exit(-1);
        }
        if (lp->enable_log)
            LOGV("R(%s[%d]): [%s]\n", __func__, i, lp->buffer);
        if (!i) {
            LOGW("R(%s[%d]): EOF (^D) received\n", __func__, i);
        }

        j = write(lp->write_to, lp->buffer, i);
        ASSURE(i == j);
        j = write(wfd, lp->buffer, i);
        ASSURE(i == j);
    }
    lp->link->status = link_exiting;
    LOGE("Worker [%s] exits, closing write-pipe: %s\n", lp->worker_name, pname);
    ASSURE(close(wfd) >= 0);
    return (void *)(intptr_t) i;
}

/* Transfers from any TCP session (via pipe) and sends to child */
void *thread_taps_to_cmd(void *arg)
{
    int i = -1, j, rfd;
    struct data_link *lp = (struct data_link *)arg;
    char *pname = switchboard_fifo_names()->out_name;
    char name[NAME_MAX];
    char *exec_name = env_get()->execute_bin;
    int n;
    for (n = strlen(exec_name); n > 0 && exec_name[n] != '/'; n--) ;
    exec_name = exec_name[n] != '/' ? &exec_name[n] : &exec_name[n + 1];
    snprintf(name, NAME_MAX, "taps_to_%s", exec_name);

    lp->enable_log = env_get()->enable_log_taps_to_cmd;
    lp->worker_name = name;
    LOGD("Worker [%s] starting. Reading pipe: %s\n", lp->worker_name, pname);

    ASSURE((rfd = open(pname, O_RDONLY)) >= 0);
    LOGD("Worker [%s] fds: (%d->%d)\n", lp->worker_name, rfd, lp->write_to);
    lp->link->status = link_up;

    while (i) {
        memset(lp->buffer, 0, LINK_BUFF_SZ);
        i = read(rfd, lp->buffer, LINK_BUFF_SZ);
        /*
           i--;
           lp->buffer[i]=0;
           lp->buffer[i-1]='\n';
         */
        if (i < 0) {
            LOGE("Failed reading from fd=[%d] in %s: " __FILE__ ":"
                 STR(__LINE__), __func__, rfd);
            exit(-1);
        }
        if (lp->enable_log)
            LOGV("R(%s[%d]): [%s]\n", __func__, i, lp->buffer);
        if (!i) {
            LOGW("R(%s[%d]): EOF (^D) received\n", __func__, i);
        }

        j = write(lp->write_to, lp->buffer, i);
        ASSURE(i == j);
    }
    lp->link->status = link_exiting;
    LOGE("Worker [%s] exits, closing write-pipe: %s\n", lp->worker_name, pname);
    ASSURE(close(rfd) >= 0);
    return (void *)(intptr_t) i;
}

struct link *link_create(void *(*worker)(void *), int fd_from, int fd_to)
{
    struct link *tlink = malloc(sizeof(struct link));
    ASSURE(tlink);

    tlink->worker = worker;

    ASSURE(tlink->data_link =
           (struct data_link *)malloc(sizeof(struct data_link)));
    ASSURE(tlink->data_link->buffer = malloc(LINK_BUFF_SZ));

    tlink->data_link->link = tlink;
    tlink->data_link->read_from = fd_from;
    tlink->data_link->write_to = fd_to;
    LOGD("%s: %d->%d\n", __func__, tlink->data_link->read_from,
         tlink->data_link->write_to);

    ASSURE(pthread_create(&(tlink->thid), NULL, tlink->worker, tlink->data_link)
           == 0);
    tlink->status = link_starting;
    return tlink;
}

void link_kill(struct link *link)
{
    LOGD("%s: %s (%d->%d)\n", __func__, link->data_link->worker_name,
         link->data_link->read_from, link->data_link->write_to);
    link->status = link_killed;
    ASSURE(pthread_cancel(link->thid) == 0);
    ASSURE(pthread_join(link->thid, NULL) == 0);
    free(link->data_link->buffer);
    free(link->data_link);
    free(link);
    link->status = link_dead;
}
