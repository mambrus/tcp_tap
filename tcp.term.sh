#!/bin/bash

THIS_SCRIPT=$(basename $(readlink -f $0))
SCRIPT_DIR=$(dirname $(readlink -f $0))
THIS_DIR=$(pwd)

TCP_TAP_PORT=${TCP_TAP_PORT-"6519"}
TCP_TAP_NICNAME=${TCP_TAP_NICNAME-"127.0.0.1"}
TCP_TAP_RETRY=${TCP_TAP_RETRY-"no"}
TCP_TAP_RETRY_DELAY=${TCP_TAP_RETRY_DELAY-"1"}
TCP_TAP_CANNONICAL=${TCP_TAP_CANNONICAL-"no"}

function test_prerequisite() {
    if [ "X$(which $1)" == "X" ]; then
        echo "Prerequisite error: $1" >&2
        echo "  Prerequisite $1 is not installed on your system" >&2
        echo "  Can't continue without dependency. Please install." >&2
        echo
        echo "For help, type: $THIS_SCRIPT -h" 1>&2
        echo
        exit 1
    fi
}

function tcp_term_help() {
    cat <<EOF
NAME
    $THIS_SCRIPT - TCP-TAP terminal

SYNOPSIS
    $THIS_SCRIPT [options]

DESCRIPTION
    $THIS_SCRIPT - TCP-TAP terminal interactive or cannonical

EXAMPLES
    $THIS_SCRIPT
    $THIS_SCRIPT -p 6666
    $THIS_SCRIPT -i bugfinder.example.com -p 1973 -r

OPTIONS

    General options
        -h          Show this help and exit
        -p PORT     TCP-TAP port number
        -i HOST     TCP-TAP host name
        -r          Keep retrying
        -d SEC      Retry delay time
        -c          Canonical mode

OPERATION
    $THIS_SCRIPT connects to a TCP "tap" session in either canonical or raw
    mode

    Optionally $THIS_SCRIPT keeps session up by retrying.

    Cannoncal mode (-c) means operation becomes like telnet, i.e.
    line-by-line oriented (cooked). This is usually not what you'd want.

    Note that $THIS_SCRIPT may be hard to kill in non-cannonical mode
    combined with keeping session up as even SIGINT is sent to TCP-TAP.
    You'd therefore want a reasonable delay-time between retries.


DEPENDENCIES
    nc -   Swiss army knife of networking (BSD variant)
    stty - change and print terminal line settings

ENVIRONMENT VARIABLES

    Name                          Current value

    TCP_TAP_PORT           (-p) : ${TCP_TAP_PORT}
    TCP_TAP_NICNAME        (-i) : ${TCP_TAP_NICNAME}
    TCP_TAP_RETRY          (-r) : ${TCP_TAP_RETRY}
    TCP_TAP_RETRY_DELAY    (-d) : ${TCP_TAP_RETRY_DELAY}
    TCP_TAP_CANNONICAL     (-c) : ${TCP_TAP_CANNONICAL}

AUTHOR
    Written by Michael Ambrus <michael@helsinova.se>, 5 Nov 2018

EOF
}

while getopts hp:i:rd: OPTION; do
    case $OPTION in
    h)
        if [ -t 1 ]; then
            tcp_term_help $0 | less -R
        else
            tcp_term_help $0
        fi
        exit 0
        ;;
    p)
        TCP_TAP_PORT="${OPTARG}"
        ;;
    i)
        TCP_TAP_NICNAME="${OPTARG}"
        ;;
    r)
        TCP_TAP_RETRY="yes"
        ;;
    r)
        TCP_TAP_CANNONICAL="yes"
        ;;
    d)
        TCP_TAP_RETRY_DELAY="${OPTARG}"
        ;;
    ?)
        echo "Syntax error: options" 1>&2
        echo "For help, type: $THIS_SCRIPT -h" 1>&2
        exit 2
        ;;
    esac
done
shift $(($OPTIND - 1))

if [ $# -ne 0 ]; then
    echo "Syntax error:" \
        "$THIS_SCRIPT takes no argument" \
        "" 1>&2
    echo "For help, type: $DTFILES_SH_INFO -h" 1>&2
    exit 2
fi

test_prerequisite nc
test_prerequisite stty

CURR_TTY_SETTINGS=$(stty -g)
if [ "X${TCP_TAP_CANNONICAL}" == "Xno" ]; then
    stty -icanon -echo
fi
OPER_TTY_SETTINGS=$(stty -g)

if [ "X${TCP_TAP_RETRY}" == "Xyes" ]; then
    for (( ;1; )); do
        stty ${OPER_TTY_SETTINGS}
        nc ${TCP_TAP_NICNAME} ${TCP_TAP_PORT}
        stty ${CURR_TTY_SETTINGS}
        sleep ${TCP_TAP_RETRY_DELAY}
    done
else
    nc ${TCP_TAP_NICNAME} ${TCP_TAP_PORT}
fi

stty ${CURR_TTY_SETTINGS}
