/***************************************************************************
 *   Copyright (C) 2011 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include "tcp-tap/switchboard.h"
#include "sig_mngr.h"
#include "tcp-tap_config.h"
#include "local.h"

#include <liblog/assure.h>
#include <liblog/log.h>

/* The maximum numbers of arguments in child we handle*/
#define MAX_ARGS 50

/* Syslog includes stderr or not */
#define INCLUDE_STDERR 1
#define NO_STDERR 0

/* Constructed argv that execve() in the forked child gets*/
char *exec_args[MAX_ARGS] = { NULL };

int main(int argc, char **argv)
{
    int i;
    int v = 0, size = argc - 1;
    char *cmd;
    struct env *env;
    int log_level = TCP_TAP_LOG_LEVEL;

    if (getenv("LOGLEVEL") != NULL) {
        log_level = liblog_getenv_loglevel();
    }
    LOGD("Setting LOGLEVEL to: %d\n", log_level);
    liblog_set_verbosity(log_level);

    liblog_syslog_config(INCLUDE_STDERR);  /* (Re-) configure sys-log */
    liblog_set_process_name(argv[0]);

    ASSURE(argc < MAX_ARGS);

    /* We're passing sockes as arguments to threads as pass by value. Make
     * sure they fit */
    ASSURE(sizeof(void *) >= sizeof(int));
    liblog_syslog_config(NO_STDERR);   /* (Re-) configure sys-log */
    liblog_set_verbosity(log_level);

    env_int();
    env = env_get();
    tty_init();

    cmd = (char *)malloc(v);
    for (i = 1; i <= size; i++) {
        cmd = (char *)realloc(cmd, (v + strlen(argv[i])));
        strcat(cmd, argv[i]);
        strcat(cmd, " ");
    }
    LOGI("PARENT: starts [%s]:%d [%s]\n", env->execute_bin, argc, cmd);
    LOGI("PARENT: socket [%s:%s]\n", env->nic_name, env->port);
    LOGI("PARENT: isatty detect {0:%d} {1:%d} {2:%d}\n", isatty(0), isatty(1),
         isatty(2));
    free(cmd);

    exec_args[0] = env->execute_bin;
    for (i = 1; i < argc; i++) {
        exec_args[i] = argv[i];
    }

    if (parent_child(argc, exec_args)) {
        LOGE("Abnornal death of child.\n");
    }

    LOGI("PARENT: Is exiting.\n");

    tty_fini();
    LOGI("PARENT: Thank you for the fish!\n");
    return 0;
}
