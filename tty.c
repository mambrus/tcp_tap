/***************************************************************************
 *   Copyright (C) 2011 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <termios.h>

#include <pthread.h>
#include <errno.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "tcp-tap_config.h"
#include "local.h"

#include <liblog/assure.h>
#include <liblog/log.h>

/* TTY attibutes to be saved and resored upon entry, exit */
struct {
    int tampered;
    struct termios tty_attr;
} static tty[3] = {
    {
     .tampered = 0}
};

void tty_raw_mode(int fd)
{
    struct termios tty_attr;

    if (fd >= 0 && fd <= 3)
        tty[fd].tampered++;

    tcgetattr(fd, &tty_attr);

    /* Set raw mode. */
    tty_attr.c_lflag &= (~(ICANON /*| ECHO */ ));
    tty_attr.c_cc[VTIME] = 0;
    tty_attr.c_cc[VMIN] = 1;

    ASSURE(tcsetattr(fd, TCSANOW, &tty_attr) != -1);
}

#ifndef HAVE_ISATTY_S
int isatty(int fd)
{
    struct winsize wsz;
    return !__syscall(SYS_ioctl, fd, TIOCGWINSZ, &wsz);
}
#endif

void tty_init()
{
    /* Back-up tty settings */
    for (int i = 0; i < 3; i++) {
        if (isatty(i))
            tcgetattr(i, &(tty[i].tty_attr));
    }
    if (isatty(0))
        tty_raw_mode(0);
}

void tty_fini()
{
    /* Restore tty settings */
    for (int i = 0; i < 3; i++) {
        if (isatty(i) && tty[i].tampered) {
            LOGI("Restore tty[%d] tampered [%d] times\n", i, tty[i].tampered);
            tcsetattr(i, TCSANOW, &(tty[i].tty_attr));
        }
    }
}
