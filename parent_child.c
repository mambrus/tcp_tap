/***************************************************************************
 *   Copyright (C) 2011 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>
#include "tcp-tap/switchboard.h"
#include "sig_mngr.h"
#include "tcp-tap_config.h"
#include "local.h"

#include <liblog/assure.h>
#include <liblog/log.h>

#define STOP_DELAY 200000
#define STOP_WAIT usleep(STOP_DELAY)

/*
 * link-worker threads
 */
static struct link *worker[n_links];
/*
 * Ditto exit monitors
 * */
static pthread_t mons[n_links];
/*
 * Swich-board is a server-socket
 */
static int server_socket = 0;

/*
 * Convert wait-status into a mask where each bit is defined.
 */
static unsigned wait_mask(int stat_val)
{
    unsigned rc = 0;

    rc |= (WIFEXITED(stat_val) << 0);
    rc |= (WEXITSTATUS(stat_val) << 1);
    rc |= (WIFSIGNALED(stat_val) << 2);
    rc |= (WTERMSIG(stat_val) << 3);
    rc |= (WIFSTOPPED(stat_val) << 4);
    rc |= (WSTOPSIG(stat_val) << 5);
    rc |= (WIFCONTINUED(stat_val) << 6);

    return rc;
}

#define LINK_KILL( LINK ) \
    if (worker[ LINK ]->status < link_exiting) \
        link_kill(worker[ LINK ]);

static void linkworkers_stop()
{
    LINK_KILL(taps_to_cmd)
        if (server_socket)
        switchboard_die(server_socket);
    server_socket = 0;
    LINK_KILL(cmd_to_stdout);
    LINK_KILL(stdin_to_cmd);
}

/* Supervise link-level workers */
static void *mon_stdin_to_cmd(void *arg)
{
    struct link *l = (struct link *)arg;
    pthread_join(l->thid, NULL);

    LOGI("Link-worker [%s] has exited\n", l->data_link->worker_name);
    LOGI("Performing post-mortem duties\n");

    /* The owning process has closed, sent a EOT (^D) to it's stdout or
     * terminated (all other SIG* cases should have been handled by the
     * thread and it should not have exited)
     *
     * Not much reason for us to still be alive if our parent process isn't.
     * Tidy up, then commit suicide.*/

    linkworkers_stop();
    STOP_WAIT;
    exit(0);

    return NULL;
}

static void *mon_cmd_to_stdout(void *arg)
{
    struct link *l = (struct link *)arg;
    pthread_join(l->thid, NULL);

    LOGI("Link-worker [%s] has exited\n", l->data_link->worker_name);
    LOGI("Performing post-mortem duties\n");

    /* The command has exited. Reason should be in log */
    linkworkers_stop();

    STOP_WAIT;
    exit(0);

    return NULL;
}

static void *mon_taps_to_cmd(void *arg)
{
    struct link *l = (struct link *)arg;
    pthread_join(l->thid, NULL);

    LOGW("Link-worker [%s] has exited\n", l->data_link->worker_name);

    /* There should be no reason except for being killed by other
     * link-monitors */
    linkworkers_stop();

    STOP_WAIT;
    exit(0);

    return NULL;
}

static void linkmonitors_start()
{
    ASSURE(pthread_create
           (&mons[stdin_to_cmd], NULL, mon_stdin_to_cmd,
            worker[stdin_to_cmd]) == 0);
    ASSURE(pthread_create
           (&mons[cmd_to_stdout], NULL, mon_cmd_to_stdout,
            worker[cmd_to_stdout]) == 0);
    ASSURE(pthread_create
           (&mons[taps_to_cmd], NULL, mon_taps_to_cmd,
            worker[taps_to_cmd]) == 0);
}

int parent_child(int argc, char *argv[])
{
    int pipe2child[2];
    int pipe2parent[2];
    int childpid, wpid, status;
    struct env *env = env_get();

    ASSURE(pipe(pipe2child) != -1);
    ASSURE(pipe(pipe2parent) != -1);

    LOGI("pipe fd:s : ch[0]=%d ch[1]=%d pa[0]=%d pa[1]=%d\n",
         PIPE_RD(pipe2child), PIPE_WR(pipe2child), PIPE_RD(pipe2parent),
         PIPE_WR(pipe2parent));

    ASSURE((childpid = fork()) >= 0);

    if (childpid == 0) {
        /* Child excutes this */
        LOGI("CHILD: isatty detect part 1 {0:%d} {1:%d} {2:%d}\n",
             isatty(0), isatty(1), isatty(2));
        LOGI("CHILD: Will execute:\n");
        for (int i = 0; i < argc; i++) {
            LOGI("   [%s]\n", argv[i]);
        }

        /* << Consider this but per signal and configurable via environment */
        sigset_t x;
        sigemptyset(&x);
        sigaddset(&x, SIGWINCH);
        sigprocmask(SIG_BLOCK, &x, NULL);

        close(0);
        ASSURE(dup(PIPE_RD(pipe2child)) != -1);

        close(1);
        ASSURE(dup(PIPE_WR(pipe2parent)) != -1);

        close(2);
        ASSURE(dup(PIPE_WR(pipe2parent)) != -1);

        close(PIPE_RD(pipe2child));
        close(PIPE_WR(pipe2child));
        close(PIPE_RD(pipe2parent));
        close(PIPE_WR(pipe2parent));

        LOGI("CHILD: isatty detect part 2 {0:%d} {1:%d} {2:%d}\n",
             isatty(0), isatty(1), isatty(2));
        execv(env->execute_bin, argv);

        /* Should never execute */
        LOGE("exec error:" __FILE__ " +" STR(__LINE__) " %s", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Parent executes this */
    LOGI("PARENT: Installing signal handlers FW to [%d]\n", childpid);
    sig_mngr_init(childpid);

    LOGI("PARENT: handle execution of:\n");
    for (int i = 0; i < argc; i++) {
        LOGI("   [%s]\n", argv[i]);
    }

    if (!server_socket) {
        close(PIPE_RD(pipe2child));
        close(PIPE_WR(pipe2parent));
        server_socket = switchboard_init(atoi(env->port), env->nic_name, 1,
                                         env->fifo_prename);
    }

    worker[cmd_to_stdout] =
        link_create(thread_cmd_to_stdout, PIPE_RD(pipe2parent), 1);
    worker[stdin_to_cmd] =
        link_create(thread_stdin_to_cmd, 0, PIPE_WR(pipe2child));
    worker[taps_to_cmd] =
        link_create(thread_taps_to_cmd, 0xDEAD, PIPE_WR(pipe2child));

    linkmonitors_start();       /* Start non-blocking worker exit monitors */

    do {
        //wpid=waitpid( /*childpid*/ /*0*/ -1, &status, WUNTRACED );
        wpid = waitpid(childpid, &status, WUNTRACED);
        LOGI("CHILD: [%d] Has new status: %d, wait-mask: 0x%02x\n", wpid,
             status, wait_mask(status));
        ASSURE(wpid >= 0);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    LOGI("CHILD: [%d] Exits due to %d wait-mask: 0x%02x\n", wpid,
         status, wait_mask(status));

    if (WIFSIGNALED(status)) {
        LOGE("CHILD: [%d] Exits due to un-caught signal\n", wpid);
        LOGE("   Please file bug-rapport");
    }
    int sid;
    sid = WTERMSIG(status);
    if (sid) {
        LOGE("CHILD: [%d] Exits due the TERM-signal %s (%d)\n",
             wpid, strsignal(sid), sid);
        return sid;
    }
    sid = WSTOPSIG(status);
    if (sid) {
        LOGE("CHILD: [%d] Exits due the STOP-signal %s (%d)\n",
             wpid, strsignal(sid), sid);
        return sid;
    }

    link_kill(worker[taps_to_cmd]);
    switchboard_die(server_socket);
    link_kill(worker[cmd_to_stdout]);
    link_kill(worker[stdin_to_cmd]);

    close(PIPE_WR(pipe2child));
    close(PIPE_RD(pipe2parent));

    return 0;
}
